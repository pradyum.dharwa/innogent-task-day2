package innogenttaskday2;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.ListIterator;
import java.util.Scanner;
import java.util.stream.Collectors;

public class FileInput1 {

	public static void main(String[] args) {
		String classfileName = "C:\\Users\\HP\\eclipse-workspace\\innogenttask\\src\\innogenttaskday2\\class.csv";
		List<Class1> classList = readClassFile(classfileName);
		classList.forEach(System.out::println);

		String studentFileName = "C:\\Users\\HP\\eclipse-workspace\\innogenttask\\src\\innogenttaskday2\\student.csv";
		List<Student> studentList = readStudentFile(studentFileName);
		studentList.forEach(System.out::println);

		String addressFileName = "C:\\Users\\HP\\eclipse-workspace\\innogenttask\\src\\innogenttaskday2\\address.csv";
		List<Address> addressList = readAddressFile(addressFileName);
		addressList.forEach(System.out::println);

		// printing student having age more than 20
		failHavingAgeMoreThan20(studentList);

		// to delete Student with givin id and also delete repective address
//		and also delete class if there is no student reamining in it;

		int student_id = 3;

		deleteStudent(studentList, student_id, addressList, classList);
		deleteStudent(studentList, 4, addressList, classList);

		System.out.println("\n...studentList \n.....");
		studentList.forEach(System.out::println);
		System.out.println("\n....addressList \n.....");
		addressList.forEach(System.out::println);
		System.out.println("\n....classList.\n.....");
		// class B is removed id of class=2 because no student present in it;
		classList.forEach(System.out::println);

		// paginating student

		// filter female student in class

		List<Student> femaleStudent = femaleStudent(studentList);

		femaleStudent.forEach(System.out::println);

		// female student
		int from = 1;
		int to = 3;
		List<Student> femaleStudentFromto = femaleStudent.subList(from - 1, to);

		// printing student from=1 to=3;

		System.out.println("\nprintlng student from=1 to=3");
		femaleStudentFromto.forEach(System.out::println);

		List<Student> femaleStudentOrederByGender = orederByName(studentList);

		System.out.println("\nprintlng student from=1 to=3 order by name");

		List<Student> femaleStudentFromto1 = femaleStudentOrederByGender.subList(from - 1, to);
		femaleStudentFromto1.forEach(System.out::println);

		List<Student> femaleStudentOrederByMarks = orederByName(studentList);

		System.out.println("\nprintlng student from=1 to=3 order by name");

		List<Student> femaleStudentFromto2 = femaleStudentOrederByMarks.subList(from - 1, to);
		femaleStudentFromto2.forEach(System.out::println);

	}

	public static List<Class1> readClassFile(String fileName) {
		ArrayList<Class1> classList = new ArrayList<>();
		Scanner scanner = null;
		try {
			scanner = new Scanner(new File(fileName));
			String classRecordSplit[];

			while (scanner.hasNext()) {
				classRecordSplit = scanner.next().split(",");
				classList.add(new Class1(Integer.parseInt(classRecordSplit[0]), classRecordSplit[1]));

			}
		} catch (Exception e) {
			// TODO: handle exception
			System.out.println(e);
		}
		return classList;
	}

	public static List<Student> readStudentFile(String fileName) {
		ArrayList<Student> studentList = new ArrayList<>();
		Scanner scanner = null;
		try {
			scanner = new Scanner(new File(fileName));
			String classRecordSplit[];

			while (scanner.hasNext()) {
				classRecordSplit = scanner.next().split(",");

				int id = Integer.parseInt(classRecordSplit[0]);
				String name = classRecordSplit[1];
				int class_id = classRecordSplit[2].charAt(0) - 64;
				int mark = Integer.parseInt(classRecordSplit[3]);
				String gender = classRecordSplit[4];
				int age = Integer.parseInt(classRecordSplit[5]);

				studentList.add(new Student(id, name, class_id, mark, gender, age));

			}
		} catch (Exception e) {
			// TODO: handle exception
			System.out.println(e);
		}
		return studentList;
	}

	public static List<Address> readAddressFile(String fileName) {
		ArrayList<Address> addressList = new ArrayList<>();
		Scanner scanner = null;
		try {
			scanner = new Scanner(new File(fileName));
			String addressRecordSplit[];

			while (scanner.hasNext()) {
				addressRecordSplit = scanner.next().split(",");
				int id = Integer.parseInt(addressRecordSplit[0]);
				int pin_code = Integer.parseInt(addressRecordSplit[1]);
				String city = addressRecordSplit[2];

				int student_id = Integer.parseInt(addressRecordSplit[3]);

				addressList.add(new Address(id, pin_code, city, student_id));

			}
		} catch (Exception e) {
			// TODO: handle exception
			System.out.println(e);
		}
		return addressList;
	}

	public static void failHavingAgeMoreThan20(List<Student> studentList) {
		System.out.println("fialed by age greater than 20");
		studentList.stream().filter(student -> student.getAge() > 20)
				.forEach(student -> System.out.println(student + "    ===  failed"));

	}

	public static void deleteStudent(List<Student> studentList, int student_id, List<Address> addressList,
			List<Class1> classList) {

		int class_id = -1;
		// removing student

		ListIterator<Student> studentIterator = studentList.listIterator();

		Student student = null;

		while (studentIterator.hasNext()) {
			student = studentIterator.next();
			if (student.getId() == student_id) {
				class_id = student.getClass_id();
				studentIterator.remove();
			}

		}

		// removing address
		ListIterator<Address> addListIterator = addressList.listIterator();
		Address address = null;

		while (addListIterator.hasNext()) {
			address = addListIterator.next();
			if (address.getStudent_id() == student_id) {
				addListIterator.remove();
			}

		}

//		removing class if no student present in that class;

		// checking student present in the class or not
		boolean present = false;
		ListIterator<Student> studentIterator1 = studentList.listIterator();
		Student student1 = null;

		while (studentIterator1.hasNext()) {
			student1 = studentIterator1.next();
			if (student1.getClass_id() == class_id) {
				present = true;
				break;
			}

		}

// removing class if student not present;
		if (!present && class_id != -1) {
			ListIterator<Class1> classIListIterator = classList.listIterator();
			while (classIListIterator.hasNext()) {
				Class1 class1 = classIListIterator.next();
				if (class1.getId() == class_id) {
					classIListIterator.remove();

				}

			}

		}
	}

	static List<Student> femaleStudent(List<Student> student) {
		return student.stream().filter(student1 -> student1.getGender().equals("F")).collect(Collectors.toList());

	}

	public static List<Student> orederByName(List<Student> studentList) {
		Collections.sort(studentList, (student1, student2) -> student1.getName().compareTo(student2.getName()));
		return studentList;
	}

	public static List<Student> orederByMarks(List<Student> studentList) {
		Collections.sort(studentList, (student1, student2) -> student2.getMarks() - student1.getMarks());
		return studentList;
	}

}
