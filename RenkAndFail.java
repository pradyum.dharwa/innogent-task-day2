package innogenttaskday2;

import java.util.ArrayList;
import java.util.Collections;

public class RenkAndFail {

	public static void main(String[] args) {

		Class1 classA = new Class1(1, "A");
		Class1 classB = new Class1(2, "B");
		Class1 classC = new Class1(3, "C");
		Class1 classD = new Class1(4, "D");
		ArrayList<Class1> classList = new ArrayList<>();
		classList.add(classA);
		classList.add(classB);
		classList.add(classC);
		classList.add(classD);

		Student stud1 = new Student(1, "stud1", classA.getId(), 88, "F", 10);
		Student stud2 = new Student(2, "stud2", classA.getId(), 70, "F", 11);
		Student stud3 = new Student(3, "stud3", classB.getId(), 88, "M", 22);
		Student stud4 = new Student(4, "stud4", classB.getId(), 55, "M", 33);
		Student stud5 = new Student(5, "stud5", classA.getId(), 30, "F", 44);
		Student stud6 = new Student(6, "stud6", classC.getId(), 30, "F", 33);
		Student stud7 = new Student(7, "stud7", classC.getId(), 10, "F", 22);
		Student stud8 = new Student(8, "stud8", classC.getId(), 0, "M", 11);

		ArrayList<Student> studentList = new ArrayList<>();
		studentList.add(stud1);
		studentList.add(stud2);
		studentList.add(stud3);
		studentList.add(stud4);
		studentList.add(stud5);
		studentList.add(stud6);
		studentList.add(stud7);
		studentList.add(stud8);
		System.out.println("List of fail student");
		studentList.stream().filter(x -> x.getMarks() < 50).forEach(y -> System.out.println(y));

		Collections.sort(studentList, (x, y) -> y.marks - x.marks);
		System.out.println("Top three rank student");

		int count = 1;
		int temp = 0;
		for (Student student : studentList) {

			if (count == 1 && temp == 0) {
				System.out.println(student.getName() + "\trank=" + count);
				temp = student.getMarks();
			} else if (count < 3) {
				if (temp == student.getMarks()) {
					System.out.println(student.getName() + "\trank=" + count);
				} else {
					++count;
					System.out.println(student.getName() + "\trank=" + count);
					temp = student.getMarks();
				}

			} else
				break;
		}

	}

}
