package innogenttaskday2;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class Main1 {

	public static void main(String[] args) {
		Class1 classA = new Class1(1, "A");
		Class1 classB = new Class1(2, "B");
		Class1 classC = new Class1(3, "C");
		Class1 classD = new Class1(4, "D");
		ArrayList<Class1> classList = new ArrayList<>();
		classList.add(classA);
		classList.add(classB);
		classList.add(classC);
		classList.add(classD);

		Student stud1 = new Student(1, "stud1", classA.getId(), 88, "F", 10);
		Student stud2 = new Student(2, "stud2", classA.getId(), 70, "F", 11);
		Student stud3 = new Student(3, "stud3", classB.getId(), 88, "M", 22);
		Student stud4 = new Student(4, "stud4", classB.getId(), 55, "M", 33);
		Student stud5 = new Student(5, "stud5", classA.getId(), 30, "F", 44);
		Student stud6 = new Student(6, "stud6", classC.getId(), 30, "F", 33);
		Student stud7 = new Student(7, "stud7", classC.getId(), 10, "F", 22);
		Student stud8 = new Student(8, "stud8", classC.getId(), 0, "M", 11);

		ArrayList<Student> studentList = new ArrayList<>();
		studentList.add(stud1);
		studentList.add(stud2);
		studentList.add(stud3);
		studentList.add(stud4);
		studentList.add(stud5);
		studentList.add(stud6);
		studentList.add(stud7);
		studentList.add(stud8);

		Address add1 = new Address(1, 452002, "indore", 1);
		Address add2 = new Address(2, 422002, "delhi", 1);
		Address add3 = new Address(3, 442002, "indore", 2);
		Address add4 = new Address(4, 462002, "delhi", 3);
		Address add5 = new Address(5, 472002, "indore", 4);
		Address add6 = new Address(6, 452002, "indore", 5);
		Address add7 = new Address(7, 452002, "delhi", 5);
		Address add8 = new Address(8, 482002, "mumbai", 6);
		Address add9 = new Address(9, 482002, "bhopal", 7);
		Address add10 = new Address(10, 482002, "indore", 8);

		ArrayList<Address> addressList = new ArrayList<>();
		addressList.add(add1);
		addressList.add(add2);
		addressList.add(add3);
		addressList.add(add4);
		addressList.add(add5);
		addressList.add(add6);
		addressList.add(add7);
		addressList.add(add8);
		addressList.add(add9);
		addressList.add(add10);

		System.out.println("\n.......pincode ==482002 .............................");
		int pin_code = 482002;
//		q1
		studentByPincode(studentList, addressList, pin_code);
		System.out.println("\n.......pincode ==482002  and female.............................");

//  q1 cases like femal student
		String gender = "F";
		studentByPincodeAndFemale(studentList, addressList, pin_code, gender);

		System.out.println("\n..............pincode ==482002  and age==22.........................");
		int age = 22;
		// q1 cases like age student
		studentByPincodeAndAge(studentList, addressList, pin_code, age);

		System.out.println("\n........... pincode ==482002  and class A........................");

		int class_id = 3;

		// q1 cases like class
		studentByPincodeAndClass_id(studentList, addressList, pin_code, class_id);
		System.out.println("\n......city === indore.......................");
		String city = "indore";
//				q2

		studentByCity(studentList, addressList, city);
		System.out.println("\n......city === indore and female.......................");

		// q2 cases like femal student
		studentByCityAndGender(studentList, addressList, city, gender);

		System.out.println("\n......city === indore and age==11.......................");

		// q2 cases like Age student
		studentByCityAndAge(studentList, addressList, city, age);
		System.out.println("\n......city === indore and class A.......................");

		// q2 cases like student in class_id
		studentByCityAndClass_id(studentList, addressList, city, class_id);

// q4    list of pass student
		System.out.println("\n......List of pass Student.......................");

		int cutOff = 50;
		List<Student> passStudent = passedStudent(studentList, cutOff);

		passStudent.forEach(System.out::println);
//q4    list of pass student and age filter
		System.out.println("\n......List of pass Student and Age==.......................");
		List<Student> passedFilterByAge = studentFilterAge(passStudent, age);
		passedFilterByAge.forEach(System.out::println);

//q4    list of pass student and gender filter
		System.out.println("\n......List of pass Student and female.......................");
		List<Student> passedFilterByGender = studentFilterGender(passStudent, gender);
		passedFilterByGender.forEach(System.out::println);

//q4    list of pass student and class filter
		System.out.println("\n......List of pass Student and class=A.......................");
		List<Student> passedFilterByClass_id = studentFilterClass_id(passStudent, class_id);
		passedFilterByClass_id.forEach(System.out::println);

//q4    list of pass student and city filter
		System.out.println("\n......List of pass Student and city=indore.......................");
		studentFilterCity(passStudent, addressList, city);
//q4    list of pass student and pincode filter
		System.out.println("\n......List of pass Student and pincode.......................");
		studentFilterPincode(passStudent, addressList, pin_code);
// q5  list of fail student 
		System.out.println("\n......List of fail Student.......................");

		List<Student> failStudentList = failedStudent(studentList, cutOff);

		failStudentList.forEach(System.out::println);
//q5  list of fail student Age filter
		System.out.println("\n......List of fail Student and Age==.......................");
		studentFilterAge(failStudentList, age).forEach(System.out::println);
		;

//q5  list of fail student gender filter
		System.out.println("\n......List of fail Student and female.......................");
		studentFilterGender(failStudentList, gender);

//q5  list of fail student and class filter
		System.out.println(" \n......List of fail Student and class=A.......................");
		studentFilterClass_id(failStudentList, class_id);

//q5  list of fail student  and city indore
		System.out.println(" \n......List of fail Student and city=indore.......................");
		studentFilterCity(failStudentList, addressList, city);
		// q6 class A student

		String findclass = "A";
// getting class A object
		Class1 filterClass = findClass(classList, findclass);

		List<Student> reqStudent = studentFilterClass_id(failStudentList, filterClass.getId());

		System.out.println("\n ..........Student in class A.........");

		reqStudent.forEach(System.out::println);

		System.out.println("\n..........Student in class A and gender");

		studentFilterGender(reqStudent, gender).forEach(System.out::println);

		System.out.println("\n..........Student in class A and age");

		studentFilterAge(reqStudent, age).forEach(System.out::println);

		System.out.println("\n..........Student in class A and city");

		studentByCity(reqStudent, addressList, city);

		System.out.println("\n..........Student in class A and pincode");

		int pincode = 452002;
		studentFilterPincode(reqStudent, addressList, pincode);

	}

	// methods

	static void studentByPincode(List<Student> studentList, List<Address> addressList, int pin_code) {
		addressList.stream().filter(address -> address.getPin_code() == pin_code).forEach(
				address1 -> studentList.stream().filter(student -> address1.getStudent_id() == student.getId()).forEach(System.out::println));

	}

	static void studentByPincodeAndFemale(List<Student> studentList, List<Address> addressList, int pin_code,
			String gender) {
		addressList.stream().filter(address -> address.getPin_code() ==pin_code)
				.forEach(address -> studentList.stream().filter(student -> address.getStudent_id() == student.getId())
						.filter(student-> student.getGender().equals(gender)).forEach(System.out::println));

	}

	static void studentByPincodeAndAge(List<Student> studentList, List<Address> addressList, int pin_code, int age) {
		addressList.stream().filter(address-> address.getPin_code() == pin_code)
				.forEach(address-> studentList.stream().filter(student -> address.getStudent_id() == student.getId())
						.filter(student-> student.getAge() == age).forEach(System.out::println));

	}

	static void studentByPincodeAndClass_id(List<Student> studentList, List<Address> addressList, int pin_code,
			int class_id) {
		addressList.stream().filter(address -> address.getPin_code() == pin_code)
				.forEach(address -> studentList.stream().filter(student -> address.getStudent_id() == student.getId())
						.filter(student -> student.getClass_id() == class_id).forEach(System.out::println));

	}

	static void studentByCity(List<Student> studentList, List<Address> addressList, String city) {
		addressList.stream().filter(address -> address.getCity().equals(city)).forEach(
				address -> studentList.stream().filter(student -> address.getStudent_id() == student.getId()).forEach(System.out::println));

	}

	static void studentByCityAndGender(List<Student> studentList, List<Address> addressList, String city,
			String gender) {
		addressList.stream().filter(address -> address.getCity().equals(city))
				.forEach(address -> studentList.stream()
						.filter(student -> (address.getStudent_id() == student.getId()) && (student.getGender().equals(gender)))
						.forEach(System.out::println));

	}

	static void studentByCityAndAge(List<Student> studentList, List<Address> addressList, String city, int age) {
		addressList.stream().filter(address -> address.getCity().equals(city)).forEach(address -> studentList.stream()
				.filter(student -> (address.getStudent_id() == student.getId()) && (student.getAge() == age)).forEach(System.out::println));

	}

	static void studentByCityAndClass_id(List<Student> studentList, List<Address> addressList, String city,
			int class_id) {
		addressList.stream().filter(address-> address.getCity().equals(city))
				.forEach(address -> studentList.stream()
						.filter(student -> ((address.getStudent_id() == student.getId()) && (student.getClass_id() == class_id)))
						.forEach(System.out::println));

	}

	static List<Student> passedStudent(List<Student> studentList, int cutOff) {
		return studentList.stream().filter(student -> student.getMarks() > cutOff).collect(Collectors.toList());
	}

	static List<Student> studentFilterAge(List<Student> listStudent, int age) {
		return listStudent.stream().filter(student -> student.getAge() == age).collect(Collectors.toList());
	}

	static List<Student> studentFilterGender(List<Student> listStudent, String gender) {
		return listStudent.stream().filter(student ->student.getGender().equals(gender)).collect(Collectors.toList());
	}

	static List<Student> studentFilterClass_id(List<Student> listStudent, int class_id) {
		return listStudent.stream().filter(student -> student.getClass_id() == class_id).collect(Collectors.toList());
	}

	static void studentFilterCity(List<Student> listStudent, List<Address> listAddress, String city) {
		listStudent.stream().forEach(student-> {

			for (Address addresses : listAddress) {
				if (addresses.getStudent_id() == student.getId() && addresses.getCity().equals(city)) {
					System.out.println(student);
				}
			}
		});

	}

	static void studentFilterPincode(List<Student> listStudent, List<Address> listAddress, int pin_code) {
		listStudent.stream().forEach(student-> {

			for (Address addresses : listAddress) {
				if (addresses.getStudent_id() == student.getId() && addresses.getPin_code() == pin_code) {
					System.out.println(student);
				}
			}
		});

	}

	static List<Student> failedStudent(List<Student> studentList, int cutOff) {
		return studentList.stream().filter(student -> student.getMarks() <= cutOff).collect(Collectors.toList());
	}

	static Class1 findClass(List<Class1> classList, String findClass) {
		return classList.stream().filter(class1-> class1.getName().equals(findClass)).findFirst().get();
	}

}
